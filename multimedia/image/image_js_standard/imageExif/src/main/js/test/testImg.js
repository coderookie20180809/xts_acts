/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let tc_020buf = new Uint8Array([0,0,0,4,0,0,0,8,1,0,0,12,1,1,1,16,1,1,1,20,2,2,2,24,3,3,3,28,4,4,4,32,5,5,5,36,6,6,6,40,7,7,7,44,9,9,8,48,10,10,10,52,12,12,12,56,14,14,13,60,16,16,15,64,18,18,17,68,20,20,19,72,22,22,22,76,25,24,24,80,27,27,27,84,30,30,29,88,33,32,32,92,36,35,35,96])
let tc_020_1buf = new Uint8Array([0,0,0,4,0,0,0,8,1,0,0,12,1,1,1,16,1,1,1,20,2,2,2,24,3,3,3,28,4,4,4,32,5,5,5,36,6,6,6,40,7,7,7,44,9,9,8,48,10,10,10,52,12,12,12,56,14,14,13,60,16,16,15,64,18,18,17,68,20,20,19,72,22,22,22,76,25,24,24,80,27,27,27,84,30,30,29,88,33,32,32,92,36,35,35,96])
let tc_021buf = new Uint8Array([0,0,0,4,0,0,0,8])
let tc_021_1buf = new Uint8Array([0,0,0,4,0,0,0,8])
let tc_022buf = new Uint8Array([0,0,0,4,0,0,0,8])

    export {tc_020buf, tc_020_1buf, tc_021buf, tc_021_1buf, tc_022buf}